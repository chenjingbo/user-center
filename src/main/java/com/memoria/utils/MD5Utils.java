package com.memoria.utils;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @program: user-center
 * @description
 * @author: memoria
 * @create: 2024-10-16 14:39
 **/
public class MD5Utils {

    private static char[] digits = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    private static Map rDigits = new HashMap(16);

    private static MessageDigest mHasher;

    private static ReentrantLock opLock = new ReentrantLock();


    static {

        for (int i = 0; i < digits.length; ++i) {

            rDigits.put(digits[i], i);

        }

        try {

            mHasher = MessageDigest.getInstance("md5");

        } catch (NoSuchAlgorithmException e) {

            throw new RuntimeException(e);

        }

    }


    public static String getEncryptedString(String content) {

        return byte2String(hash(content));

    }


    private static byte[] hash(String str) {

        opLock.lock();

        try {

            byte[] bt = mHasher.digest(str.getBytes("UTF-8"));

            if (null == bt || bt.length != 16) {

                throw new IllegalArgumentException("md5 need");

            }

            return bt;

        } catch (UnsupportedEncodingException e) {

            throw new RuntimeException("unsupported utf-8 encoding", e);

        } finally {

            opLock.unlock();

        }

    }


    private static String byte2String(byte[] bt) {

        int l = bt.length;

        char[] out = new char[l << 1];

        for (int i = 0, j = 0; i < l; i++) {

            out[j++] = digits[(0xF0 & bt[i]) >>> 4];

            out[j++] = digits[0x0F & bt[i]];

        }

        return new String(out);

    }

}
