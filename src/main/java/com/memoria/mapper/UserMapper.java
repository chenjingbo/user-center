package com.memoria.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.memoria.model.domain.User;

/**
* @author admin
* @description 针对表【user(用户表)】的数据库操作Mapper
* @createDate 2024-04-15 17:26:31
* @Entity generator.domain.User
*/
public interface UserMapper extends BaseMapper<User> {

}




