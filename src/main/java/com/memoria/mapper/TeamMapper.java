package com.memoria.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.memoria.model.domain.Team;

/**
* @author admin
* @description 针对表【team(队伍表)】的数据库操作Mapper
* @createDate 2024-08-14 10:45:04
* @Entity generator.domain.Team
*/
public interface TeamMapper extends BaseMapper<Team> {

}




