package com.memoria.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.memoria.model.domain.UserTeam;

/**
* @author admin
* @description 针对表【user_team(用户队伍关系)】的数据库操作Mapper
* @createDate 2024-08-14 10:47:15
* @Entity generator.domain.UserTeam
*/
public interface UserTeamMapper extends BaseMapper<UserTeam> {

}




