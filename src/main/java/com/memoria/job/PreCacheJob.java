package com.memoria.job;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.memoria.mapper.UserMapper;
import com.memoria.model.domain.User;
import com.memoria.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @program: user-center
 * @description 缓存预热任务
 * @author: memoria
 * @create: 2024-08-13 10:28
 **/
@Component
@Slf4j
public class PreCacheJob {

    @Resource
    private UserService userService;

    @Resource
    private RedissonClient redissonClient;

    @Resource
    private RedisTemplate<String,Object> redisTemplate;


    //每天执行，预热推荐用户(每小时执行一次)
    @Scheduled(cron = "0 0 */1 * * ?")
    //0/60 * * * * ?
    public void doCacheRecommendUser(){
        //实现分布式锁,写入redis锁
        RLock lock = redissonClient.getLock("memoria:precachejob:docache:lock");

        //这里尝试获取锁，返回boolean类型，如果是true表示获取到了锁
        // 第一个参数是等待时间，第二个参数是执行时间，第三个是时间单位
        try {

            //只有一个线程能获取到锁
            if(lock.tryLock(0,30000L,TimeUnit.MILLISECONDS)){
                System.out.println("getLock:"+Thread.currentThread().getId());

                //重点用户
                List<Long> mainUserList = Arrays.asList(2L);
                //如果有缓存，直接读缓存
                for (Long userId : mainUserList) {

                    QueryWrapper<User> queryWrapper = new QueryWrapper<>();
                    Page<User> userPage = userService.page(new Page<>(1, 20), queryWrapper);

                    String redisKey = String.format("memoria:user:recommend:%s",userId);
                    ValueOperations<String, Object> valueOperations = redisTemplate.opsForValue();
                    //写缓存,这里建议用trycatch包裹一下
                    try {

                        valueOperations.set(redisKey,userPage,300000, TimeUnit.MILLISECONDS);
                    } catch (Exception e) {
                        log.error("redis set key error",e);
                    }
                }

            }else {
                System.out.println("getLockFalse"+Thread.currentThread().getId());
            }

        } catch (InterruptedException e) {
            log.error("doCacheRecommendUser error",e);
        } finally {
            //判断当前这个锁是不是当前这个线程加的锁(只能释放自己的锁)
            if(lock.isHeldByCurrentThread()){
                System.out.println("unLock:"+Thread.currentThread().getId());
                lock.unlock();
            }
        }


    }

}
