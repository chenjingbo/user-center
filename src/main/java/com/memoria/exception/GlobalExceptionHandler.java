package com.memoria.exception;

import com.memoria.common.BaseResponse;
import com.memoria.common.ErrorCode;
import com.memoria.common.ResultUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @program: user-center
 * @description 全局异常处理器
 * @author: memoria
 * @create: 2024-04-21 23:40
 **/

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {
    /**
     * ExceptionHandler方法只捕获BusinessException这个异常
     * @return
     */
    @ExceptionHandler(BusinessException.class)
    public BaseResponse businessExceptionHandler(BusinessException e){
        log.error("businessException:"+e.getMessage(),e);
        return ResultUtils.error(e.getCode(),e.getMessage(),e.getDescription());
    }


    @ExceptionHandler(RuntimeException.class)
    public BaseResponse runtimeExceptionHandler(RuntimeException e){
        log.error("runtimeException",e);
        return ResultUtils.error(ErrorCode.SYSTEM_ERROR,e.getMessage(),"");
    }
}
