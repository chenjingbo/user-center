package com.memoria.exception;

import com.memoria.common.ErrorCode;

/**
 * @program: user-center
 * @description 自定义异常类(给原本的异常类扩充了2个字段)
 * @author: memoria
 * @create: 2024-04-21 23:16
 **/
public class BusinessException extends RuntimeException{

    private final int code;

    private final String description;


    /**
     * 这里调用父级的构造函数，message传递
     * @param message
     * @param code
     * @param description
     */
    public BusinessException(String message, int code, String description) {
        super(message);
        this.code = code;
        this.description = description;
    }

    public BusinessException(ErrorCode errorCode) {
        super(errorCode.getMessage());
        this.code = errorCode.getCode();
        this.description = errorCode.getDescription();
    }


    public BusinessException(ErrorCode errorCode,String description) {
        super(errorCode.getMessage());
        this.code = errorCode.getCode();
        this.description = description;
    }

    public int getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }
}
