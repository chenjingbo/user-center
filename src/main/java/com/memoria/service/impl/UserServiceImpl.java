package com.memoria.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.memoria.common.ErrorCode;
import com.memoria.exception.BusinessException;
import com.memoria.mapper.UserMapper;
import com.memoria.model.domain.User;
import com.memoria.model.vo.UserVO;
import com.memoria.service.UserService;

import com.memoria.utils.AlgorithmUtils;
import javafx.util.Pair;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.DigestUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.memoria.contant.UserConstant.ADMIN_ROLE;
import static com.memoria.contant.UserConstant.USER_LOGIN_STATE;

/**
 * @author 用户服务实现类
 * @description 针对表【user(用户表)】的数据库操作Service实现
 * @createDate 2024-04-15 17:26:31
 */
@Service
@Slf4j
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Resource
    private UserMapper userMapper;

    /**
     * 盐值，加密用
     */
    private static final String SALT = "memoria";


    @Override
    public long userRegister(String userAccount, String userPassword, String checkPassword, String invitationCode) {
        //1.校验
        if (StringUtils.isAnyBlank(userAccount, userPassword, checkPassword, invitationCode)) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "参数为空");
        }
        if (userAccount.length() < 4) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "用户账号过短");
        }
        if (userPassword.length() < 8 || checkPassword.length() < 8) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "用户密码过短");
        }

        if (invitationCode.length() > 5) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "邀请码过长");
        }
        //账户不能包含特殊字符
        String validPattern = "[`~!@#$%^&*()+=|{}':;',\\\\[\\\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]";
        Matcher matcher = Pattern.compile(validPattern).matcher(userAccount);
        if (matcher.find()) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "账户不能包含特殊字符");
        }
        //密码和校验密码不同
        if (!userPassword.equals(checkPassword)) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "密码和校验密码不同");
        }

        //账号不能重复，应该去查询数据库有没有相同的账户(这块放在最后再检验，因为要查询数据库)
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_account", userAccount);
        long count = userMapper.selectCount(queryWrapper);
        if (count > 0) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "账号不能重复");
        }

        //邀请码不能重复
        queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("invitation_code", invitationCode);
        count = userMapper.selectCount(queryWrapper);
        if (count > 0) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "邀请码不能重复");
        }

        //2.对密码进行加密
        String encryptPassword = DigestUtils.md5DigestAsHex((SALT + userPassword).getBytes());
        //3.插入数据
        User user = new User();
        user.setUserAccount(userAccount);
        user.setUserPassword(encryptPassword);
        user.setInvitationCode(invitationCode);
        user.setUserName(userAccount);
        boolean result = this.save(user);
        //如果保存失败，return-1
        if (!result) {
            throw new BusinessException(ErrorCode.SYSTEM_ERROR, "保存账户失败");
        }
        return user.getId();
    }


    @Override
    public User userLogin(String userAccount, String userPassword, HttpServletRequest httpServletRequest) {

        //1.校验
        if (StringUtils.isAnyBlank(userAccount, userPassword)) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "参数不能为空");
        }
        if (userAccount.length() < 4) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "账号过短");
        }
        if (userPassword.length() < 8) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "密码过短");
        }

        //账户不能包含特殊字符
        String validPattern = "[`~!@#$%^&*()+=|{}':;',\\\\[\\\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]";
        Matcher matcher = Pattern.compile(validPattern).matcher(userAccount);
        if (matcher.find()) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "账号不能包含特殊字符");
        }


        //2.对密码进行加密，进行用户的密码查询
        final String SALT = "memoria";
        String encryptPassword = DigestUtils.md5DigestAsHex((SALT + userPassword).getBytes());


        //3.查询用户是否存在
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_account", userAccount);
        queryWrapper.eq("user_password", encryptPassword);
        User user = userMapper.selectOne(queryWrapper);
        if (user == null) {
            log.info("user login failed,userAccount cannot match userPassword");
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "账号或密码错误");
        }

        //4.用户脱敏
        User safeUser = getSafetyUser(user);

        //5.记录用户的登录态,session底层就是一个map
        httpServletRequest.getSession().setAttribute(USER_LOGIN_STATE, safeUser);

        return safeUser;

    }

    /**
     * 用户脱敏
     *
     * @param user
     * @return
     */
    @Override
    public User getSafetyUser(User user) {
        if (user == null) {
            return null;
        }
        User safeUser = new User();
        safeUser.setId(user.getId());
        safeUser.setUserName(user.getUserName());
        safeUser.setUserAccount(user.getUserAccount());
        safeUser.setAvatarUrl(user.getAvatarUrl());
        safeUser.setGender(user.getGender());
        safeUser.setPhone(user.getPhone());
        safeUser.setEmail(user.getEmail());
        safeUser.setUserStatus(user.getUserStatus());
        safeUser.setCreateTime(user.getCreateTime());
        safeUser.setUserRole(user.getUserRole());
        safeUser.setInvitationCode(user.getInvitationCode());
        safeUser.setTags(user.getTags());
        return safeUser;
    }

    /**
     * 用户注销
     *
     * @param request
     */
    @Override
    public int userLogout(HttpServletRequest request) {
        //移除登录态
        request.getSession().removeAttribute(USER_LOGIN_STATE);
        return 1;
    }

    /**
     * 根据标签搜索用户(内存过滤)
     *
     * @param tagList 用户要拥有的标签
     * @return
     */
    @Override
    public List<User> searchUsersByTags(List<String> tagList) {
        if (CollectionUtils.isEmpty(tagList)) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }

        QueryWrapper<User> queryWrapper = new QueryWrapper<>();

        //Long startTime = System.currentTimeMillis();

        //1.先查询所有用户
        List<User> userList = userMapper.selectList(queryWrapper);
        //创建一个json序列化对象，谷歌的
        Gson gson = new Gson();

        /*2.在内存中判断是否包含要求的标签,用stream的filter遍历
        集合，取出集合中user的json字符串，然后反序列化成set集合，
        和要查询标签的比较，只要有匹配就返回true，放到map里面并返回结果*/
        return userList.stream().filter(user -> {
            //取出标签的json字符串
            String tagsStr = user.getTags();
            //判断是否为空
            if (StringUtils.isBlank(tagsStr)) {
                return false;
            }
            //fromJson用法，第一个是要解析的对象，第二个参数是传要解析成什么类型
            //这里是把字符串给反序列化成对象 这个是再把对象转成字符串的方法gson.toJson(tagNamelist)
            Set<String> tagNameSet = gson.fromJson(tagsStr, new TypeToken<Set<String>>() {
            }.getType());
            //判空，如果是空new一个新的集合
            tagNameSet = Optional.ofNullable(tagNameSet).orElse(new HashSet<>());

            //遍历用户传进来的所有标签
            for (String tagName : tagList) {
                //如果标签不在set集合中存在
                if (!tagNameSet.contains(tagName)) {
                    return false;
                }
            }
            return true;
        }).map(this::getSafetyUser).collect(Collectors.toList());
        //log.info("memory query time ="+(System.currentTimeMillis()-startTime));
    }

    /**
     * 更新用户
     *
     * @param user
     * @return
     */
    @Override
    public int updateUser(User user, User loginUser) {
        //查询要更新的用户ID
        Long userId = user.getId();

        //如果是管理员，允许更新任意用户
        //如果不是管理员，只允许更新自己的,改别的用户会抛异常
        if (!isAdmin(loginUser) &&!userId.equals(loginUser.getId())){

            throw new BusinessException(ErrorCode.NO_AUTH);
        }
        User oldUser = userMapper.selectById(userId);
        if (oldUser==null){
            throw new BusinessException(ErrorCode.PARAMS_NULL_ERROR);
        }

        return userMapper.updateById(user);
    }

    /**
     * 获取当前用户信息并鉴权
     *
     * @param request
     * @return
     */
    @Override
    public User getLoginUser(HttpServletRequest request) {
        if (request == null) {
            return null;
        }
        Object user = request.getSession().getAttribute(USER_LOGIN_STATE);

        if (user == null) {
            throw new BusinessException(ErrorCode.NO_AUTH);
        }

        return (User) user;
    }

    @Override
    public boolean isAdmin(HttpServletRequest request) {
        //鉴权
        //仅管理员可查询
        Object userObj = request.getSession().getAttribute(USER_LOGIN_STATE);
        User user = (User) userObj;
        if (user == null || user.getUserRole() != ADMIN_ROLE) {
            return false;
        }
        return true;

    }

    @Override
    public boolean isAdmin(User loginUser) {
        //鉴权

        if (loginUser == null || loginUser.getUserRole() != ADMIN_ROLE) {
            return false;
        }
        return true;

    }


    /**
     * 根据传入的数量匹配标签相似的用户(优化版)
     * @param num
     * @param loginUser
     * @return
     */
    @Override
    public List<User> optimizeMatchUsers(long num, User loginUser) {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        //对查询条件进行优化
        queryWrapper.select("id","tags");
        queryWrapper.isNotNull("tags");
        queryWrapper.last("and tags !='[]' ");

        //根据条件查询用户
        List<User> userList = this.list(queryWrapper);
        //获取登录用户的标签
        String loginUserTags = loginUser.getTags();
        //把字符串转成string数组
        Gson gson = new Gson();
        //利用gson的fromJson转换
        List<String> tagList = gson.fromJson(loginUserTags, new TypeToken<List<String>>() {
        }.getType());
        //遍历每一个用户，然后和当前登录用户匹配
        //用户列表的下标=>相似度

        List<Pair<User,Long>> list = new ArrayList<>();


        for (int i = 0; i < userList.size(); i++) {
            User user = userList.get(i);
            String tags = user.getTags();
            //判断是否为空，为空就跳过此次循环
            //无标签或者为当前用户自己
            if (StringUtils.isBlank(tags) || user.getId().equals(loginUser.getId())){
                continue;
            }
            List<String> userTagList =gson.fromJson(user.getTags(), new TypeToken<List<String>>() {
            }.getType());
            //接下来计算分数,两两匹配
            long score = AlgorithmUtils.minDistance(tagList, userTagList);
            list.add(new Pair<>(user,score));
        }


        //按编辑距离由小到大排序
        List<Pair<User,Long>> topUserPairList = list.stream()
                .sorted((a,b)-> (int) (a.getValue() - b.getValue()))
                .limit(num)
                .collect(Collectors.toList());


        //原本顺序的userId列表
        List<Long> userVOList = topUserPairList.stream().map(pair -> pair.getKey().getId()).collect(Collectors.toList());

        QueryWrapper<User> userQueryWrapper = new QueryWrapper<>();
        userQueryWrapper.in("id",userVOList);

        //根据id分组，每个组只有一个用户
        Map<Long, List<User>> userIdListMap = this.list(userQueryWrapper)
                .stream().map(user -> getSafetyUser(user)).collect(Collectors.groupingBy(User::getId));

        List<User> finalUserList = new ArrayList<>();
        for (Long userID : userVOList) {
            finalUserList.add(userIdListMap.get(userID).get(0));
        }

        return finalUserList;
    }



    /**
     * 根据传入的数量匹配标签相似的用户
     * @param num
     * @param loginUser
     * @return
     */
    @Override
    public List<User> matchUsers(long num, User loginUser) {
        //查询所有用户
        List<User> userList = this.list();
        //获取登录用户的标签
        String loginUserTags = loginUser.getTags();
        //把字符串转成string数组
        Gson gson = new Gson();
        //利用gson的fromJson转换
        List<String> tagList = gson.fromJson(loginUserTags, new TypeToken<List<String>>() {
        }.getType());
        //遍历每一个用户，然后和当前登录用户匹配，这里需要用一个能排序的数据结构,选择SortedMap
        //用户列表的下标=>相似度



        SortedMap<Integer,Long> indexDistanceMap = new TreeMap<>(Comparator.comparingInt(a -> a));

        for (int i = 0; i < userList.size(); i++) {
            User user = userList.get(i);
            String tags = user.getTags();
            //判断是否为空，为空就跳过此次循环
            //无标签
            if (StringUtils.isBlank(tags)){
                continue;
            }
            List<String> userTagList =gson.fromJson(user.getTags(), new TypeToken<List<String>>() {
            }.getType());
            //接下来计算分数,两两匹配
            long score = AlgorithmUtils.minDistance(tagList, userTagList);

            indexDistanceMap.put(i,score);
        }

        List<User> userVOList = new ArrayList<>();
        int i =0;

        //这里的代码是用来学习的
        //用entrySet的取法entrySet.for,可以循环拿到每一个entry
        for (Map.Entry<Integer, Long> entry : indexDistanceMap.entrySet()) {
            if (i>num){
                break;
            }
            User user = userList.get(entry.getKey());
            userVOList.add(user);
            System.out.println( user.getId()+ ":"+ entry.getKey()+":"+entry.getValue());
            i++;

        }

        //最开始的做法
        /*//用keySet可以拿到下标
        List<Integer> maxDistanceIndexList = indexDistanceMap.keySet().stream().limit(num).collect(Collectors.toList());
        //循环集合，得到每一个下标index,然后在userList中根据下标去取出来并放在新的集合里面
        List<User> userVOList = maxDistanceIndexList.stream().map(index ->{
           return getSafetyUser(userList.get(index));
        }).collect(Collectors.toList());*/


        return userVOList;
    }





    /**
     * 根据标签搜索用户(SQL查询版)
     *
     * @param tagList 用户要拥有的标签
     * @return
     */
    @Deprecated //过时
    private List<User> searchUsersByTagsBySQL(List<String> tagList) {
        if (CollectionUtils.isEmpty(tagList)) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }

        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        //先进行一次数据库连接再开始计时
        //userMapper.selectCount(null);
        //Long startTime = System.currentTimeMillis();
        //拼接and查询
        //like '%java%' and like '%Python%'
        for (String tagName : tagList) {
            queryWrapper = queryWrapper.like("tags", tagName);
        }
        List<User> userList = userMapper.selectList(queryWrapper);

        //log.info("sql query time = "+(System.currentTimeMillis()-startTime));

        return userList.stream().map(this::getSafetyUser).collect(Collectors.toList());
    }


}




