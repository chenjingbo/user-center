package com.memoria.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.memoria.common.ErrorCode;
import com.memoria.exception.BusinessException;
import com.memoria.model.domain.User;
import com.memoria.model.domain.UserTeam;
import com.memoria.model.dto.TeamQuery;
import com.memoria.model.enums.TeamStatusEnum;
import com.memoria.model.request.TeamJoinRequest;
import com.memoria.model.request.TeamQuitRequest;
import com.memoria.model.request.TeamUpdateRequest;
import com.memoria.model.vo.TeamUserVO;
import com.memoria.model.vo.UserVO;
import com.memoria.service.TeamService;
import com.memoria.model.domain.Team;
import com.memoria.mapper.TeamMapper;
import com.memoria.service.UserService;
import com.memoria.service.UserTeamService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.BeanUtils;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
* @author admin
* @description 针对表【team(队伍表)】的数据库操作Service实现
* @createDate 2024-08-14 10:45:04
*/
@Service
public class TeamServiceImpl extends ServiceImpl<TeamMapper, Team>
    implements TeamService {


    @Resource
    private UserTeamService userTeamService;


    @Resource
    private UserService userService;


    @Resource
    private RedissonClient redissonClient;



    @Override
    @Transactional(rollbackFor = Exception.class)
    public long addTeam(Team team, User loginUser) {

        //1.请求参数是否为空？
        if(team==null){
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        //2.是否登录，未登录不允许创建
        if(loginUser == null){
            throw new BusinessException(ErrorCode.NOT_LOGIN);
        }
        final long userId =loginUser.getId();
        //3.校验信息
        //	1.队伍人数>1且<=20,这里取出来是包装类，给个默认值
        int maxNum = Optional.ofNullable(team.getMaxNum()).orElse(0);
        if(maxNum <1||maxNum >20 ){
            throw new BusinessException(ErrorCode.PARAMS_ERROR,"队伍人数不满足要求");
        }
        //	2.队伍标题<=20
        String name = team.getName();
        if(StringUtils.isBlank(name)|| name.length() >20){
            throw new BusinessException(ErrorCode.PARAMS_ERROR,"标题不符合要求");
        }
        //	3.描述<=512
        String description = team.getDescription();
        if (StringUtils.isNotBlank(description) && description.length() >512){
            throw new BusinessException(ErrorCode.PARAMS_ERROR,"描述长度不符");
        }

        //	5.是否公开(int)，不传默认为0(公开)
        Integer status = Optional.ofNullable(team.getStatus()).orElse(0);
        TeamStatusEnum statusEnum =TeamStatusEnum.getEnumByValue(status);
        if (statusEnum == null ){
            throw new BusinessException(ErrorCode.PARAMS_ERROR,"队伍状态不满足要求");
        }
        //	6.如果status是加密状态，一定要有密码，且密码<=32
        String password = team.getPassword();
        if (statusEnum.equals(TeamStatusEnum.SECRET) && (StringUtils.isBlank(password)||password.length()>32)){
            throw new BusinessException(ErrorCode.PARAMS_ERROR,"密码设置不正确");
        }

        //	7.超时时间>当前时间
        Date expireTime = team.getExpireTime();
        if(new Date().after(expireTime)){
            throw new BusinessException(ErrorCode.PARAMS_ERROR,"超时时间大于当前时间");
        }
        //	8.校验用户最多创建5个队伍
        //todo 有bug，可能同时创建100个队伍
        QueryWrapper<Team> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id",userId);
        long hasTeamNum = this.count(queryWrapper);
        if(hasTeamNum>=5){
            throw new BusinessException(ErrorCode.SYSTEM_ERROR,"用户最多创建5个队伍");
        }

        //4.插入队伍信息到队伍表
        team.setId(null);
        team.setUserId(userId);

        boolean result = this.save(team);
        Long teamId = team.getId();

        if(!result || team.getId() == null){
            throw new BusinessException(ErrorCode.SYSTEM_ERROR,"创建队伍失败");
        }


        //5.插入用户=》队伍关系到关系表
        UserTeam userTeam = new UserTeam();

        userTeam.setUserId(userId);
        userTeam.setTeamId(teamId);
        userTeam.setJoinTime(new Date());

        result =userTeamService.save(userTeam);
        if (!result){
            throw new BusinessException(ErrorCode.SYSTEM_ERROR,"创建队伍失败");
        }

        return teamId;
    }

    @Override
    public List<TeamUserVO> listTeams(TeamQuery teamQuery,boolean isAdmin) {
        QueryWrapper<Team> queryWrapper = new QueryWrapper<>();
        //1.组合查询条件
        if (teamQuery != null){
            Long id = teamQuery.getId();
            if (id!=null&& id > 0){
                queryWrapper.eq("id",id);
            }
            List<Long> idList = teamQuery.getIdList();
            if (CollectionUtils.isNotEmpty(idList)){
                queryWrapper.in("id",idList);
            }

            //可以通过某个关键词同时对名称和描述进行查询
            String searchText = teamQuery.getSearchText();
            if (StringUtils.isNotBlank(searchText)){
                queryWrapper.and(qw-> qw.like("name",searchText).or().like("description",searchText));
            }
            String name = teamQuery.getName();

            if (StringUtils.isNotBlank(name)){
                queryWrapper.like("name",name);
            }

            String description = teamQuery.getDescription();
            if (StringUtils.isNotBlank(description)){
                queryWrapper.like("description",description);
            }
            Integer maxNum = teamQuery.getMaxNum();
            //查询最大人数相等的
            if (maxNum!=null && maxNum > 0){
                queryWrapper.eq("max_num",maxNum);
            }
            //根据创建人查询
            Long userId = teamQuery.getUserId();
            if (userId!=null && userId >0){
                queryWrapper.eq("user_id",userId);
            }
            //根据状态来查询
            Integer status = teamQuery.getStatus();
            TeamStatusEnum statusEnum = TeamStatusEnum.getEnumByValue(status);
            if (statusEnum == null){
                statusEnum = TeamStatusEnum.PUBLIC;
            }
            /*if (!isAdmin && !statusEnum.equals(TeamStatusEnum.PUBLIC)){
                throw new BusinessException(ErrorCode.NO_AUTH);
            }*/

            queryWrapper.eq("status",statusEnum.getValue());

        }
        //不展示已过期的队伍
        // expire_time is not null or expire_time > now()
        queryWrapper.and(qw->qw.gt("expire_time",new Date()).or().isNull("expire_time"));

        List<Team> teamList = this.list(queryWrapper);

        if (CollectionUtils.isEmpty(teamList)){
            return new ArrayList<>();
        }

        List<TeamUserVO> teamUserVOList = new ArrayList<>();


        //关联查询创建人的用户信息
        for (Team team : teamList) {
            Long userId = team.getUserId();
            if (userId == null){
                continue;
            }
            //关联查询创建人信息
            User user = userService.getById(userId);
            //脱敏用户信息
            User safetyUser = userService.getSafetyUser(user);

            TeamUserVO teamUserVO = new TeamUserVO();
            BeanUtils.copyProperties(team,teamUserVO);
            UserVO userVO = new UserVO();
            if (safetyUser!=null){
                BeanUtils.copyProperties(safetyUser,userVO);
                teamUserVO.setCreateUser(userVO);
            }

            teamUserVOList.add(teamUserVO);

        }

        return teamUserVOList;
    }


    /**
     * 更新队伍
     * @param teamUpdateRequest
     * @return
     */
    @Override
    public boolean updateTeam(TeamUpdateRequest teamUpdateRequest,User loginUser) {
        if(teamUpdateRequest==null){
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        Long id = teamUpdateRequest.getId();
        if (id == null || id <=0){
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        Team oldTeam = this.getById(id);
        if (oldTeam==null){
            throw new BusinessException(ErrorCode.PARAMS_NULL_ERROR);
        }
        //只有管理员或者队伍的创建者可以修改
        if (!oldTeam.getUserId().equals(loginUser.getId()) && !userService.isAdmin(loginUser) ){
            throw new BusinessException(ErrorCode.NO_AUTH);
        }
        TeamStatusEnum statusEnum = TeamStatusEnum.getEnumByValue(teamUpdateRequest.getStatus());
        if(statusEnum.equals(TeamStatusEnum.SECRET)){
            if (StringUtils.isBlank(teamUpdateRequest.getPassword())){
                throw new BusinessException(ErrorCode.PARAMS_ERROR,"加密房间必须要有密码");
            }
        }
        Team updateTeam = new Team();
        BeanUtils.copyProperties(teamUpdateRequest,updateTeam);

        return this.updateById(updateTeam);
    }


    /**
     * 加入队伍
     * @param teamJoinRequest
     * @return
     */
    @Override
    public boolean joinTeam(TeamJoinRequest teamJoinRequest,User loginUser) {

        if (teamJoinRequest == null){
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }


        Long teamId = teamJoinRequest.getTeamId();
        if (teamId == null || teamId <= 0 ){
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        Team team = getTeamById(teamId);
        if (team.getExpireTime()!=null && team.getExpireTime().before(new Date())){
            throw new BusinessException(ErrorCode.PARAMS_ERROR,"队伍已过期");
        }
        Integer status = team.getStatus();
        TeamStatusEnum statusEnum = TeamStatusEnum.getEnumByValue(status);
        if (TeamStatusEnum.PRIVATE.equals(statusEnum)){
            throw new BusinessException(ErrorCode.PARAMS_ERROR,"禁止加入私有队伍");
        }
        String password = teamJoinRequest.getPassword();
        if (TeamStatusEnum.SECRET.equals(statusEnum)){
            if (StringUtils.isBlank(password)|| !password.equals(team.getPassword())){
                throw new BusinessException(ErrorCode.PARAMS_ERROR,"密码错误");
            }
        }

        //用户最多加入5个队伍
        Long userId = loginUser.getId();

        //分布式锁,只有一个线程能获取到锁,给用户加入队伍设置分布式锁
        RLock lock = redissonClient.getLock("memoria:join_team");

        try {

            //抢到锁并执行,这里要保证所有的线程都抢到锁,如果抢不到还要回去

            while(true){

                if(lock.tryLock(0,30000L, TimeUnit.MILLISECONDS)){

                    System.out.println("getLock:"+Thread.currentThread().getId());

                    QueryWrapper<UserTeam> userTeamQueryWrapper = new QueryWrapper<>();
                    userTeamQueryWrapper.eq("user_id",userId);
                    long hasJoinNum = userTeamService.count(userTeamQueryWrapper);
                    if (hasJoinNum > 5){
                        throw new BusinessException(ErrorCode.PARAMS_ERROR,"最多创建和加入5个队伍");
                    }
                    //不能重复加入已加入的队伍
                    userTeamQueryWrapper = new QueryWrapper<>();
                    userTeamQueryWrapper.eq("user_id",userId);
                    userTeamQueryWrapper.eq("team_id",teamId);
                    long hasJoin = userTeamService.count(userTeamQueryWrapper);
                    if (hasJoin > 0){
                        throw new BusinessException(ErrorCode.PARAMS_ERROR,"用户已加入该队伍");
                    }


                    //已加入队伍的人数
                    long teamHasJoinNum = this.countTeamUserByTeamId(teamId);
                    if (teamHasJoinNum>=team.getMaxNum()){
                        throw new BusinessException(ErrorCode.PARAMS_ERROR,"队伍已满");
                    }

                    //修改队伍信息
                    UserTeam userTeam = new UserTeam();

                    userTeam.setUserId(userId);
                    userTeam.setTeamId(teamId);
                    userTeam.setJoinTime(new Date());
                    return userTeamService.save(userTeam);

                }
            }


        } catch (InterruptedException e) {
            log.error("doCacheRecommendUser error",e);
            return false;
        } finally {
            //判断当前这个锁是不是当前这个线程加的锁(只能释放自己的锁)
            if(lock.isHeldByCurrentThread()){
                System.out.println("unLock:"+Thread.currentThread().getId());
                lock.unlock();
            }
        }


    }


    /**
     * 获取某队伍当前人数
     * @param teamId
     * @return
     */
    private long countTeamUserByTeamId(Long teamId) {
        QueryWrapper<UserTeam> userTeamQueryWrapper = new QueryWrapper<>();
        userTeamQueryWrapper.eq("team_id", teamId);
        long teamHasJoinNum = userTeamService.count(userTeamQueryWrapper);
        return teamHasJoinNum;
    }


    /**
     * 离开队伍
     * @param teamQuitRequest
     * @param loginUser
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean quitTeam(TeamQuitRequest teamQuitRequest, User loginUser) {
        if (teamQuitRequest == null){
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        Long teamId = teamQuitRequest.getTeamId();
        Team team = getTeamById(teamId);
        Long userId = loginUser.getId();
        UserTeam queryUserTeam = new UserTeam();
        queryUserTeam.setTeamId(teamId);
        queryUserTeam.setUserId(userId);
        QueryWrapper<UserTeam> queryWrapper = new QueryWrapper<>(queryUserTeam);

        long count = userTeamService.count(queryWrapper);
        if (count == 0){
            throw new BusinessException(ErrorCode.PARAMS_ERROR,"未加入队伍");
        }
        long teamHasJoinNum = this.countTeamUserByTeamId(teamId);
        //队伍只剩1人,解散
        if (teamHasJoinNum == 1){
            //删除队伍和所有加入队伍的关系
            this.removeById(teamId);
            QueryWrapper<UserTeam> userTeamQueryWrapper = new QueryWrapper<>();
            userTeamQueryWrapper.eq("team_id", teamId);
            return  userTeamService.remove(userTeamQueryWrapper);
        }else {
            //是队长
            //队伍还剩最少2人
            if(team.getUserId().equals(userId)){
                //把队伍转移给最早加入的用户
                //查询已加入队伍的所有用户和加入时间
                //只需根据时间查询最早加入的2条数据
                QueryWrapper<UserTeam> userTeamQueryWrapper = new QueryWrapper<>();
                userTeamQueryWrapper.eq("team_id", teamId);
                userTeamQueryWrapper.last("order by join_time asc limit 2 ");
                List<UserTeam> userTeamList = userTeamService.list(userTeamQueryWrapper);

                if (CollectionUtils.isEmpty(userTeamList) || userTeamList.size() <= 1){
                    throw new BusinessException(ErrorCode.SYSTEM_ERROR);
                }
                UserTeam nextUserTeam = userTeamList.get(1);
                Long nextTeamLeaderId = nextUserTeam.getUserId();
                //更新当前队伍的队长
                Team updateTeam = new Team();
                updateTeam.setId(teamId);
                updateTeam.setUserId(nextTeamLeaderId);
                boolean result = this.updateById(updateTeam);
                if (!result){
                    throw new BusinessException(ErrorCode.SYSTEM_ERROR,"更新队长失败");
                }

            }
            //移除当前退出队伍的用户关系
            return userTeamService.remove(queryWrapper);
        }

    }


    /**
     * 根据id获取队伍信息
     * @param teamId
     * @return
     */
    private Team getTeamById(Long teamId) {
        if (teamId == null|| teamId <= 0 ){
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        Team team = this.getById(teamId);
        if (team == null){
            throw new BusinessException(ErrorCode.PARAMS_NULL_ERROR,"队伍不存在");
        }
        return team;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean deleteTeam(long id,User loginUser) {
        Team team = getTeamById(id);
        //校验是否是队长
        if (!team.getUserId().equals(loginUser.getId())){
            throw new BusinessException(ErrorCode.NO_AUTH,"无权限");
        }
        //移除所有加入队伍的关联信息
        QueryWrapper<UserTeam> userTeamQueryWrapper = new QueryWrapper<>();
        userTeamQueryWrapper.eq("team_id", team.getId());
        boolean remove = userTeamService.remove(userTeamQueryWrapper);
        if (!remove){
            throw new BusinessException(ErrorCode.SYSTEM_ERROR,"删除队伍关联信息失败");
        }
        //删除队伍
        return this.removeById(team);
    }
}




