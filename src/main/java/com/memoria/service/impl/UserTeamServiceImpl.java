package com.memoria.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.memoria.service.UserTeamService;
import com.memoria.model.domain.UserTeam;
import com.memoria.mapper.UserTeamMapper;
import org.springframework.stereotype.Service;

/**
* @author admin
* @description 针对表【user_team(用户队伍关系)】的数据库操作Service实现
* @createDate 2024-08-14 10:47:15
*/
@Service
public class UserTeamServiceImpl extends ServiceImpl<UserTeamMapper, UserTeam>
        implements UserTeamService {

}




