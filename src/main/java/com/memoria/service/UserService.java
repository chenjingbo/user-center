package com.memoria.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.memoria.common.BaseResponse;
import com.memoria.model.domain.User;
import com.memoria.model.vo.UserVO;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static com.memoria.contant.UserConstant.ADMIN_ROLE;
import static com.memoria.contant.UserConstant.USER_LOGIN_STATE;


/**
 * @author admin
 * @description 针对表【user(用户表)】的数据库操作Service
 * @createDate 2024-04-15 17:26:31
 */
public interface UserService extends IService<User> {




    /**
     * 用户注册
     *
     * @param userAccount   账户
     * @param userPassword  密码
     * @param checkPassword 校验密码
     * @param invitationCode 邀请码
     * @return 新用户id
     */
    long userRegister(String userAccount, String userPassword, String checkPassword,String invitationCode);

    /**
     * 用户登录
     *
     * @param userAccount        账户
     * @param userPassword       密码
     * @param httpServletRequest
     * @return 返回脱敏后的用户信息
     */
    User userLogin(String userAccount, String userPassword ,HttpServletRequest httpServletRequest);

    /**
     * 用户脱敏
     * @param user
     * @return
     */
    User getSafetyUser(User user);

    /**
     * 用户注销功能
     * @param request
     * @return
     */
    int userLogout(HttpServletRequest request);


    /**
     * 根据标签搜索用户
     *
     * @param tagList
     * @return
     */
    List<User> searchUsersByTags(List<String> tagList);


    /**
     * 更新用户信息
     * @param user
     * @return
     */
    int updateUser(User user,User loginUser);

    /**
     * 获取当前登录用户信息
     * @return
     */
     User getLoginUser(HttpServletRequest request);


    /**
     * 是否为管理员
     * @param request
     * @return
     */
     boolean isAdmin(HttpServletRequest request);

    /**
     * 是否为管理员(重载方法)
      * @param loginUser
     * @return
     */
    boolean isAdmin(User loginUser);


    /** 匹配用户
     * @param num
     * @param LoginUser
     * @return
     */
    List<User> matchUsers(long num,User LoginUser);


    /** 匹配用户(优化后的方法)
     * @param num
     * @param LoginUser
     * @return
     */
    List<User> optimizeMatchUsers(long num,User LoginUser);
}
