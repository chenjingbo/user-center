package com.memoria.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.memoria.model.domain.UserTeam;

/**
* @author admin
* @description 针对表【user_team(用户队伍关系)】的数据库操作Service
* @createDate 2024-08-14 10:47:15
*/
public interface UserTeamService extends IService<UserTeam> {

}
