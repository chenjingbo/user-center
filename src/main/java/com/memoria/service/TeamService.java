package com.memoria.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.memoria.model.domain.Team;
import com.memoria.model.domain.User;
import com.memoria.model.dto.TeamQuery;
import com.memoria.model.request.TeamJoinRequest;
import com.memoria.model.request.TeamQuitRequest;
import com.memoria.model.request.TeamUpdateRequest;
import com.memoria.model.vo.TeamUserVO;

import java.util.List;

/**
* @author admin
* @description 针对表【team(队伍表)】的数据库操作Service
* @createDate 2024-08-14 10:45:04
*/
public interface TeamService extends IService<Team> {

    /**
     * 创建队伍
     * @param team
     * @param loginUser
     * @return
     */
    long addTeam(Team team, User loginUser);

    /**
     * 搜索队伍
     * @param teamQuery
     * @param isAdmin
     * @return
     */
    List<TeamUserVO> listTeams(TeamQuery teamQuery,boolean isAdmin);


    /**
     * 更新队伍
     * @param teamUpdateRequest
     * @param loginUser
     * @return
     */
    boolean updateTeam(TeamUpdateRequest teamUpdateRequest,User loginUser);

    /**
     * 加入队伍
     * @param teamJoinRequest
     * @return
     */
    boolean joinTeam(TeamJoinRequest teamJoinRequest,User loginUser);


    /**
     * 离开队伍
     * @param teamQuitRequest
     * @param loginUser
     * @return
     */
    boolean quitTeam(TeamQuitRequest teamQuitRequest, User loginUser);


    /**
     * 删除队伍
     * @param id
     * @param loginUser
     * @return
     */
    boolean deleteTeam(long id, User loginUser);
}
