package com.memoria.once;
import java.util.Date;

import com.memoria.mapper.UserMapper;
import com.memoria.model.domain.User;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import javax.annotation.Resource;

/**
 * @program: user-center
 * @description
 * @author: memoria
 * @create: 2024-06-16 23:55
 **/
@Component
public class InsertUsers {

    @Resource
    private UserMapper userMapper;

    /**
     * 批量插入用户
     */
    //@Scheduled(initialDelay = 5000,fixedRate = Long.MAX_VALUE)
    public void doInsertUsers(){
        //帮助计时
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        final int num = 1000;
        for (int i = 0; i < num; i++) {
            User user = new User();
            user.setUserName("假用户");
            user.setUserAccount("fakeyupi");
            user.setAvatarUrl("https://web-memoria.oss-cn-guangzhou.aliyuncs.com/%E5%86%92%E9%99%A9%E5%B2%9B.jpeg");
            user.setGender(0);
            user.setUserPassword("123");
            user.setTags("[]");
            user.setPhone("123");
            user.setEmail("");
            user.setUserStatus(0);
            user.setUserRole(0);
            user.setInvitationCode("111");
            userMapper.insert(user);
        }
        stopWatch.stop();
        System.out.println(stopWatch.getTotalTimeMillis());
    }

    public static void main(String[] args) {
        new InsertUsers().doInsertUsers();
    }

}
