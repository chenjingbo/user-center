package com.memoria.once;

import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.util.Date;

/**
 * 用户中心excel导入对象
 */
@Data
public class UserInfo {


    /**
     * 主键
     */
    @ExcelProperty("成员编号")
    private String invitationCode;

    /**
     * 用户名
     */
    @ExcelProperty("成员昵称")
    private String userName;


}