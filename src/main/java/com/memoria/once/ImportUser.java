package com.memoria.once;

import com.alibaba.excel.EasyExcel;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @program: user-center
 * @description 导入星球用户到数据库
 * @author: memoria
 * @create: 2024-05-18 13:45
 **/
public class ImportUser {
    public static void main(String[] args) {
        String fileName = "D:\\IdeaProjects\\user-center\\src\\main\\resources\\testExcel.xlsx";
        // 这里 需要指定读用哪个class去读，然后读取第一个sheet 同步读取会自动finish
        List<UserInfo> userInfoList = EasyExcel.read(fileName).head(UserInfo.class).sheet().doReadSync();
        System.out.println("总数 = " + userInfoList.size());
        //根据用户名称分组
        Map<String, List<UserInfo>> listMap =
                userInfoList.stream()
                        .filter(userInfo -> StringUtils.isNotEmpty(userInfo.getUserName()))
                        .collect(Collectors.groupingBy(UserInfo::getUserName));
        for (Map.Entry<String, List<UserInfo>> stringListEntry : listMap.entrySet()) {
            if (stringListEntry.getValue().size() > 1) {
                System.out.println("username = " + stringListEntry.getKey());
                System.out.println("1");
            }
        }
    }
}
