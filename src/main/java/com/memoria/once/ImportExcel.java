package com.memoria.once;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.read.listener.PageReadListener;

import java.util.List;

/**
 * @program: user-center
 * @description 导入excel
 * @author: memoria
 * @create: 2024-05-17 17:49
 **/
public class ImportExcel {


    /**
     * 最简单的读
     * <p>
     * 1. 创建excel对应的实体对象 参照
     * <p>
     * 2. 由于默认一行行的读取excel，所以需要创建excel一行一行的回调监听器，参照
     * <p>
     * 3. 直接读即可
     */
    public static void main(String[] args) {
        // 写法1：JDK8+ ,不用额外写一个DemoDataListener
        // since: 3.0.0-beta1
        String fileName = "D:\\IdeaProjects\\user-center\\src\\main\\resources\\testExcel.xlsx";
        readByListener(fileName);
        synchronousRead(fileName);

    }

    /**
     * 监听器读
     * @param fileName
     */
    public static  void readByListener(String fileName){

        // 这里默认每次会读取100条数据 然后返回过来 直接调用使用数据就行
        // 具体需要返回多少行可以在`PageReadListener`的构造函数设置
        EasyExcel.read(fileName, UserInfo.class, new TableListener()).sheet().doRead();
    }

    /**
     * 同步读
     * @param fileName
     */
    public  static void synchronousRead(String fileName) {
        // 这里 需要指定读用哪个class去读，然后读取第一个sheet 同步读取会自动finish
        List<UserInfo> list = EasyExcel.read(fileName).head(UserInfo.class).sheet().doReadSync();
        for (UserInfo data : list) {
            System.out.println(data);
        }

    }





}
