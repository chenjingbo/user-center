package com.memoria.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.memoria.common.BaseResponse;
import com.memoria.common.ErrorCode;
import com.memoria.common.ResultUtils;
import com.memoria.exception.BusinessException;
import com.memoria.model.domain.User;
import com.memoria.model.request.UserLoginRequest;
import com.memoria.model.request.UserRegisterRequest;
import com.memoria.model.vo.UserVO;
import com.memoria.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static com.memoria.contant.UserConstant.ADMIN_ROLE;
import static com.memoria.contant.UserConstant.USER_LOGIN_STATE;

/**
 * @program: user-center
 * @description 用户接口
 * @author: memoria
 * @create: 2024-04-16 18:19
 **/

@RestController
@RequestMapping("/user")
@Slf4j
public class UserController {

    @Autowired
    private UserService userService;

    @Resource
    private RedisTemplate<String,Object> redisTemplate;


    @PostMapping("/update")
    public BaseResponse<Integer> updateUser(@RequestBody User user,HttpServletRequest request){
        //1.校验参数是否为空
        if(user==null){
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        //校验参数是否只有id
        int idFieldCount = 0;
        //利用反射获取对象的类
        Class<?> clazz = user.getClass();
        //getDeclaredFields获取对象的所有私有字段
        for (Field field : clazz.getDeclaredFields()) {
            //开启权限
            field.setAccessible(true);
            try {
                //除了id和serialVersionUID之外至少要有一个值
                if (field.get(user) != null && !field.getName().equals("id")){
                    idFieldCount++;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }finally {
                field.setAccessible(false);
            }
        }
        if (idFieldCount == 1){
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        User loginUser = userService.getLoginUser(request);


        //3.触发更新
        int result = userService.updateUser(user,loginUser);


        return ResultUtils.success(result);
    }



    @PostMapping("register")
    public BaseResponse<Long> userRegister(@RequestBody UserRegisterRequest userRegisterRequest){
        if (userRegisterRequest==null){
            //return ResultUtils.error(ErrorCode.PARAMS_ERROR);
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }

        String userAccount = userRegisterRequest.getUserAccount();
        String userPassword = userRegisterRequest.getUserPassword();
        String checkPassword = userRegisterRequest.getCheckPassword();
        String invitationCode = userRegisterRequest.getInvitationCode();
        //做一个简单的校验
        if (StringUtils.isAnyBlank(userAccount,userPassword,checkPassword,invitationCode)){
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        long id = userService.userRegister(userAccount, userPassword,checkPassword,invitationCode);
        return ResultUtils.success(id);
    }

    @PostMapping("login")
    public BaseResponse<User> userLogin(@RequestBody UserLoginRequest userLoginRequest, HttpServletRequest request){
        if (userLoginRequest==null){
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }

        String userAccount = userLoginRequest.getUserAccount();
        String userPassword = userLoginRequest.getUserPassword();

        //做一个简单的校验
        if (StringUtils.isAnyBlank(userAccount,userPassword)){
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        User user = userService.userLogin(userAccount, userPassword, request);
        return ResultUtils.success(user);

    }


    @PostMapping("logout")
    public BaseResponse<Integer> userLogout(HttpServletRequest request){
        if (request==null){
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        int result = userService.userLogout(request);
        return ResultUtils.success(result);

    }



    @GetMapping("current")
    public BaseResponse<User> getCurrentUser(HttpServletRequest request){
        //获得登录态
        Object userObj = request.getSession().getAttribute(USER_LOGIN_STATE);
        User currentUser = (User)userObj;
        if (currentUser == null){
            throw new BusinessException(ErrorCode.NOT_LOGIN);
        }
        long userId = currentUser.getId();
        //todo 校验用户是否合法
        User user = userService.getById(userId);
        User result = userService.getSafetyUser(user);

        return ResultUtils.success(result);
    }


    @GetMapping("/search")
    public BaseResponse<List<User>> userSearch(String username,HttpServletRequest request){
        if(!userService.isAdmin(request)){
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotBlank(username)){
            queryWrapper.like("user_name",username);
        }
        List<User> list = userService.list(queryWrapper);
        //这里把密码隐藏,直接调用脱敏方法
        List<User> collect = list.stream().map(user -> userService.getSafetyUser(user)).collect(Collectors.toList());

        return ResultUtils.success(collect);

    }


    // todo 推荐多个，未实现
    /**
     * 主页推荐
     * @param request
     * @return
     */
    @GetMapping("/recommend")
    public BaseResponse<Page<User>> recommendUsers(long pageSize,long pageNum,HttpServletRequest request){
        User loginUser = userService.getLoginUser(request);
        //如果有缓存，直接读缓存
        String redisKey = String.format("memoria:user:recommend:%s",loginUser.getId());

        ValueOperations<String, Object> valueOperations = redisTemplate.opsForValue();
        Page<User> userPage = (Page<User>) valueOperations.get(redisKey);

        if(userPage != null){
            return ResultUtils.success(userPage);
        }

        //无缓存，查数据库
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        //第一个是页号，就是第几页 ，这里传入页号就会自动计算起始索引
        // 起始索引,就是假设每页显示10条记录，要查询第2页的数据，就是(2-1)*10 = 10 代码(pageNum-1)*pageSize，也就是跳过前面10条的意思
        // 第二个是查询记录数
        userPage = userService.page(new Page<>(pageNum,pageSize),queryWrapper);
        //写缓存,这里建议用trycatch包裹一下
        try {
            valueOperations.set(redisKey,userPage,300000, TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            log.error("redis set key error",e);
        }

        return ResultUtils.success(userPage);

    }


    @GetMapping("/search/tags")
    public BaseResponse<List<User>> searchUsersByTags(@RequestParam(required = false) List<String> tagList){

        if (CollectionUtils.isEmpty(tagList)){
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }

        List<User> users = userService.searchUsersByTags(tagList);
        return ResultUtils.success(users);
    }



    @PostMapping("/delete")
    public BaseResponse<Boolean> deleteUser(@RequestBody long id,HttpServletRequest request){
        if (!userService.isAdmin(request)){
            throw new BusinessException(ErrorCode.NO_AUTH);
        }
        if (id<=0){
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }

        boolean result = userService.removeById(id);

        return ResultUtils.success(result);

    }

    /**
     * 获取最匹配的用户
     *
     * @param num
     * @param request
     * @return
     */
    @GetMapping("/match")
    public BaseResponse<List<User>> matchUsers(long num, HttpServletRequest request){
        if (num <=0 || num > 20) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        User user = userService.getLoginUser(request);
        return  ResultUtils.success(userService.optimizeMatchUsers(num,user));

    }





}
