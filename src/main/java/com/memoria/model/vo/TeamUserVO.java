package com.memoria.model.vo;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @program: user-center
 * @description 队伍和用户信息封装类(脱敏)
 * @author: memoria
 * @create: 2024-08-20 09:46
 **/
@Data
public class TeamUserVO implements Serializable {

    private static final long serialVersionUID = 2175722801689322274L;
    /**
     * 主键
     */

    private Long id;

    /**
     * 队伍名称
     */
    private String name;

    /**
     * 描述
     */
    private String description;

    /**
     * 最大人数
     */
    private Integer maxNum;

    /**
     * 过期时间
     */
    private Date expireTime;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 状态 0-公开，1 -私有，2-加密
     */
    private Integer status;



    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;


    /**
     * 用户列表
     */
    List<UserVO> userList;


    /**
     * 创建人用户信息
     */
    private UserVO createUser;

    /**
     * 是否已加入队伍
     */
    private boolean hasJoin =false;


    /**
     * 已加入的用户数
     */
    private Integer hasJoinNum;




}
