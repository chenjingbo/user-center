package com.memoria.model.vo;


import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @program: user-center
 * @description 用户包装类
 * @author: memoria
 * @create: 2024-08-20 09:52
 **/
@Data
public class UserVO implements Serializable {

    /**
     * 主键
     */
    private Long id;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 登录账号
     */
    private String userAccount;

    /**
     * 头像
     */
    private String avatarUrl;

    /**
     *
     */
    private Integer gender;



    /**
     * 标签列表json
     */
    private String tags;

    /**
     * 电话
     */
    private String phone;

    /**
     *
     */
    private String email;

    /**
     * 用户状态 0正常
     */
    private Integer userStatus;

    /**
     * 用户角色 0普通用户，1管理员
     */
    private Integer userRole;


    /**
     * 邀请码
     */
    private String invitationCode;


    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;




    private static final long serialVersionUID = 1L;

}
