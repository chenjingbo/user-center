package com.memoria.model.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.memoria.common.PageRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;
import java.util.List;

/**
 * @program: user-center
 * @description 队伍查询封装类
 * @author: memoria
 * @create: 2024-08-14 11:36
 **/
@EqualsAndHashCode(callSuper = true)
@Data
public class TeamQuery extends PageRequest {
    /**
     * 主键
     */
    @TableId(type = IdType.AUTO)
    private Long id;


    /**
     * id 列表
     */
    List<Long> idList;

    /**
     * 搜索关键词(同时对队伍名称和描述搜索)
     */
    private String searchText;

    /**
     * 队伍名称
     */
    private String name;

    /**
     * 描述
     */
    private String description;

    /**
     * 最大人数
     */
    private Integer maxNum;



    /**
     * 用户id
     */
    private Long userId;

    /**
     * 状态 0-公开，1 -私有，2-加密
     */
    private Integer status;



}
