package com.memoria.model.request;

import lombok.Data;

import java.io.Serializable;

/**
 * @program: user-center
 * @description 用户登录请求体
 * @author: memoria
 * @create: 2024-04-16 19:07
 **/
@Data
public class UserLoginRequest implements Serializable {

    private static final long serialVersionUID = -8634037065045076425L;

    private String userAccount;
    private String userPassword;

}
