package com.memoria.model.request;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;

/**
 * @program: user-center
 * @description
 * @author: memoria
 * @create: 2024-08-20 12:27
 **/
@Data
public class TeamUpdateRequest implements Serializable {
    private static final long serialVersionUID = 2693350868247762644L;

    /**
     * 队伍id
     */
    private Long id;

    /**
     * 队伍名称
     */
    private String name;

    /**
     * 描述
     */
    private String description;



    /**
     * 过期时间
     */
    private Date expireTime;



    /**
     * 状态 0-公开，1 -私有，2-加密
     */
    private Integer status;

    /**
     * 密码
     */
    private String password;



    
}
