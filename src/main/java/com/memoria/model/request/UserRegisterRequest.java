package com.memoria.model.request;

import lombok.Data;

import java.io.Serializable;

/**
 * @program: user-center
 * @description 用户注册请求体
 * @author: memoria
 * @create: 2024-04-16 18:33
 **/
@Data
public class UserRegisterRequest implements Serializable {


    private static final long serialVersionUID = 2262125939568301444L;

    private String userAccount;
    private String userPassword;
    private String checkPassword;
    private String invitationCode;

}
