package com.memoria.model.request;

import lombok.Data;

/**
 * @program: user-center
 * @description
 * @author: memoria
 * @create: 2024-08-20 15:15
 **/
@Data
public class TeamJoinRequest {
    /**
     * 队伍id
     */
    private Long teamId;

    /**
     * 密码
     */
    private String password;
}
