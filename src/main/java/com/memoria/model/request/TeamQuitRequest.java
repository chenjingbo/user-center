package com.memoria.model.request;

import lombok.Data;

/**
 * @program: user-center
 * @description 用户退出队伍请求体
 * @author: memoria
 * @create: 2024-08-20 15:15
 **/
@Data
public class TeamQuitRequest {
    /**
     * 队伍id
     */
    private Long teamId;


}
