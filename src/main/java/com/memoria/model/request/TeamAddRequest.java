package com.memoria.model.request;


import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @program: user-center
 * @description
 * @author: memoria
 * @create: 2024-08-14 18:00
 **/
@Data
public class TeamAddRequest implements Serializable {


    private static final long serialVersionUID = 8285689581493704779L;
    /**
     * 队伍名称
     */
    private String name;

    /**
     * 描述
     */
    private String description;

    /**
     * 最大人数
     */
    private Integer maxNum;

    /**
     * 过期时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date expireTime;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 状态 0-公开，1 -私有，2-加密
     */
    private Integer status;

    /**
     * 密码
     */
    private String password;


}
