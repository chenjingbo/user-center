package com.memoria.common;

import lombok.Data;

import java.io.Serializable;

/**
 * @program: user-center
 * @description 通用返回类
 * @author: memoria
 * @create: 2024-04-21 22:14
 **/
@Data
public class BaseResponse<T> implements Serializable {

    private static final long serialVersionUID = -4529941354192772355L;

    private int code;
    private T data;
    private String message;
    private String description;

    /**
     * 构造函数
     */
    public BaseResponse(int code, T data, String message,String description) {
        this.code = code;
        this.data = data;
        this.message = message;
        this.description = description;
    }

    /**
     * 不传message的构造函数
     * @param code
     * @param data
     */
    public BaseResponse(int code, T data,String message) {
        this(code,data,message,"");
    }

    public BaseResponse(int code, T data) {
        this(code,data,"","");
    }

    public BaseResponse(ErrorCode errorCode){
        this(errorCode.getCode(),null,errorCode.getMessage(),errorCode.getDescription());

    }




}
