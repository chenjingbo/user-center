package com.memoria.common;

import lombok.Data;

import java.io.Serializable;

/**
 * @program: user-center
 * @description 通用分页请求参数
 * @author: memoria
 * @create: 2024-08-14 12:03
 **/
@Data
public class PageRequest implements Serializable {


    private static final long serialVersionUID = -8227954029431184306L;
    /**
     * 页面大小
     */
    protected int pageSize =  10;

    /**
     * 当前是第几页
     */
    protected  int pageNum = 1;
}
