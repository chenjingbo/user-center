package com.memoria.common;

import lombok.Data;

/**
 * @program: user-center
 * @description 错误码
 * @author: memoria
 * @create: 2024-04-21 22:50
 **/


public enum ErrorCode {

    SUCCESS(0,"ok",""),
    PARAMS_ERROR(4000,"请求参数错误",""),
    PARAMS_NULL_ERROR(4001,"请求数据为空",""),
    NOT_LOGIN(4010,"未登录",""),
    NO_AUTH(4011,"没有权限",""),
    FORBIDDEN(4031,"禁止访问",""),
    SYSTEM_ERROR(5000,"系统内部异常","");

    /**
     * 状态码
     */
    private final int code;
    /**
     * 状态码信息
     */
    private final String message;
    /**
     * 状态码描述
     */
    private final String description;

    ErrorCode(int code, String message, String description) {
        this.code = code;
        this.message = message;
        this.description = description;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String getDescription() {
        return description;
    }
}
