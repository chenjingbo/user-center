package com.memoria.common;

import lombok.Data;

import java.io.Serializable;

/**
 * @program: user-center
 * @description 通用删除请求参数
 * @author: memoria
 * @create: 2024-08-14 12:03
 **/
@Data
public class DeleteRequest implements Serializable {


    private static final long serialVersionUID = -8227954029431184306L;


    private long id;
}
