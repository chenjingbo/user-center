package com.memoria.contant;

/**
 * @program: user-center
 * @description   用户常量
 * @author: memoria  接口里面的属性默认是public static
 * @create: 2024-04-17 13:09
 **/

public interface UserConstant {

    /**
     * 用户登录态键
     */
    String USER_LOGIN_STATE = "userLoginState";

    //---权限---
    /**
     * 默认权限
     */
    int DEFAULT_ROLE = 0;

    /**
     * 管理员权限
     */
    int ADMIN_ROLE = 1;

}
