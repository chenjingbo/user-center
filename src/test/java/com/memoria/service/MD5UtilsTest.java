package com.memoria.service;

import com.memoria.utils.AlgorithmUtils;
import com.memoria.utils.MD5Utils;
import org.junit.jupiter.api.Test;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @program: user-center
 * @description 钉钉MD5工具类测试
 * @author: memoria
 * @create: 2024-10-07 00:18
 **/
public class MD5UtilsTest {

    @Test
    void test(){


        String appId="56656884c1044333aeeabd5fb679cc66";
        String appSecret ="58459526b3f1b0182145742a695fbaa3";


        String startDate = "2024-10-15 00:00:00";
        String endDate= "2024-10-16 00:00:00";

        String dingUserId = "12391";

        /*SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        long startDateTimestamp =0L;

        long endDateTimestamp =0L;

        try {
            Date date1 = sdf.parse(startDate);
            Date date2 = sdf.parse(endDate);
            startDateTimestamp = date1.getTime();
            endDateTimestamp =date2.getTime();
            System.out.println(startDateTimestamp);
            System.out.println(endDateTimestamp);

        } catch (Exception e) {
            e.printStackTrace();
        }*/

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        // 将字符串解析为LocalDateTime
        LocalDateTime localDateTime1 = LocalDateTime.parse(startDate, formatter);
        LocalDateTime localDateTime2 = LocalDateTime.parse(endDate, formatter);
        // 将LocalDateTime转换为时间戳
        long startDateTimestamp = localDateTime1.toEpochSecond(ZoneOffset.UTC) * 1000; // 转换为毫秒

        long endDateTimestamp =  localDateTime2.toEpochSecond(ZoneOffset.UTC) * 1000; // 转换为毫秒


        String str = "appId=" +appId+
                "&appSecret=" +appSecret+
                "&dingUserId="+dingUserId+
                "&endDate="+endDateTimestamp +
                "&startDate="+startDateTimestamp;
        System.out.println(str);
        String sign = MD5Utils.getEncryptedString(str);
        System.out.println(sign);
    }



}
