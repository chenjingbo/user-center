package com.memoria.service;

import com.memoria.utils.AlgorithmUtils;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

/**
 * @program: user-center
 * @description 算法工具类测试
 * @author: memoria
 * @create: 2024-10-07 00:18
 **/
public class AlgorithmUtilsTest {

    @Test
    void test(){
        String str = "鱼皮是狗";
        String str2 = "鱼皮不是狗";
        String str3 = "鱼皮是狗不是狗";
        String str4 = "鱼皮是猫";
        //计算字符串的最小编辑距离
        //1
        int score1 = AlgorithmUtils.minDistance(str, str2);
        //3
        int score2 = AlgorithmUtils.minDistance(str, str3);

        System.out.println(score1);
        System.out.println(score2);
    }

    @Test
    void testCompareTags(){
        List<String> list1 = Arrays.asList("Java", "大一", "男");
        List<String> list2 = Arrays.asList("Java", "大一", "女");
        List<String> list3 = Arrays.asList("python", "大二", "女");
        //计算字符串的最小编辑距离
        //1
        int score1 = AlgorithmUtils.minDistance(list1, list2);
        //3
        int score2 = AlgorithmUtils.minDistance(list1, list3);

        System.out.println(score1);
        System.out.println(score2);
    }

}
