package com.memoria.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.memoria.model.domain.User;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.redisson.api.RList;
import org.redisson.api.RLock;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

import javax.annotation.Resource;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * @program: user-center
 * @description redisson测试类
 * @author: memoria
 * @create: 2024-08-13 16:12
 **/
@SpringBootTest
@Slf4j
public class RedissonTest {

    @Resource
    private RedissonClient redissonClient;


    @Test
    void test(){
        //list,数据存在本地jvm内存中
        List<String> list = new ArrayList<>();
        list.add("yupi");

        System.out.println("list:"+list.get(0));
        //list.remove(0);

        //数据存在redis的内存中
        RList<String> rList = redissonClient.getList("test-list");
        rList.add("yupi");
        System.out.println("rlist:"+rList.get(0));
        rList.remove(0);
        //map
        Map<String,Integer> map = new HashMap<>();
        map.put("yupi",1);
        System.out.println(map.get("yupi"));

        RMap<String, Integer> rMap = redissonClient.getMap("test-map");
        rMap.put("yupi",1);
        System.out.println("rMap:"+rMap.get("yupi"));


        //set

        //stack



    }

    //测试看门狗的锁过期机制
    @Test
    void Test2(){

        //实现分布式锁,写入redis锁
        RLock lock = redissonClient.getLock("memoria:precachejob:docache:lock");

        //这里尝试获取锁，返回boolean类型，如果是true表示获取到了锁
        // 第一个参数是等待时间，第二个参数是执行时间，第三个是时间单位
        try {

            //只有一个线程能获取到锁
            if(lock.tryLock(0,-1, TimeUnit.MILLISECONDS)){
                Thread.sleep(300000);
                System.out.println("getLock:"+Thread.currentThread().getId());

            }else {
                System.out.println("getLockFalse"+Thread.currentThread().getId());
            }

        } catch (InterruptedException e) {
            log.error("doCacheRecommendUser error",e);
        } finally {
            //判断当前这个锁是不是当前这个线程加的锁(只能释放自己的锁)
            if(lock.isHeldByCurrentThread()){
                System.out.println("unLock:"+Thread.currentThread().getId());
                lock.unlock();
            }
        };
    }

}
