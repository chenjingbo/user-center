package com.memoria.service;
import java.util.Date;

import com.memoria.model.domain.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

import javax.annotation.Resource;

/**
 * @program: user-center
 * @description
 * @author: memoria
 * @create: 2024-07-06 17:51
 **/
@SpringBootTest
public class RedisTest {

    @Resource
    private RedisTemplate redisTemplate;


    @Test
    void test(){
        ValueOperations valueOperations = redisTemplate.opsForValue();
        //增
        valueOperations.set("string","dog");
        valueOperations.set("int",1);
        valueOperations.set("double",2.0);
        User user = new User();
        user.setId(1L);
        user.setUserName("kami");
        valueOperations.set("user",user);

        //查
        Object obj = valueOperations.get("string");
        Assertions.assertTrue("dog".equals((String)obj));
        obj = valueOperations.get("int");
        Assertions.assertTrue(1 ==(Integer) obj );
        obj= valueOperations.get("double");
        Assertions.assertTrue(2.0 ==(Double) obj );
        System.out.println(valueOperations.get("user"));

    }
}
