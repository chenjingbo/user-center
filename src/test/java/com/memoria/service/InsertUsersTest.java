package com.memoria.service;

import com.memoria.mapper.UserMapper;
import com.memoria.model.domain.User;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;
import org.springframework.util.StopWatch;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.*;

/**
 * @program: user-center
 * @description
 * @author: memoria
 * @create: 2024-06-17 00:26
 **/
@SpringBootTest
public class InsertUsersTest {

    @Resource
    private UserMapper userMapper;

    @Resource
    private  UserService userService;

    /**
     * 批量插入用户(循环原始版)
     */
    @Test
    public void doInsertUsers(){
        //帮助计时
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        final int num = 1000;

        for (int i = 0; i < num; i++) {
            User user = new User();
            user.setUserName("假用户");
            user.setUserAccount("fakeyupi");
            user.setAvatarUrl("https://web-memoria.oss-cn-guangzhou.aliyuncs.com/%E5%86%92%E9%99%A9%E5%B2%9B.jpeg");
            user.setGender(0);
            user.setUserPassword("123");
            user.setTags("[]");
            user.setPhone("123");
            user.setEmail("");
            user.setUserStatus(0);
            user.setUserRole(0);
            user.setInvitationCode("111");
            userMapper.insert(user);
        }

        stopWatch.stop();
        System.out.println(stopWatch.getTotalTimeMillis());
    }


    /**
     * 批量插入用户(mybatis批量插入)
     */
    @Test
    public void doInsertUsers1(){
        //帮助计时
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        final int num = 1000;
        List<User> userList = new ArrayList<>();
        for (int i = 0; i < num; i++) {
            User user = new User();
            user.setUserName("假用户");
            user.setUserAccount("fakeyupi");
            user.setAvatarUrl("https://web-memoria.oss-cn-guangzhou.aliyuncs.com/%E5%86%92%E9%99%A9%E5%B2%9B.jpeg");
            user.setGender(0);
            user.setUserPassword("123");
            user.setTags("[]");
            user.setPhone("123");
            user.setEmail("");
            user.setUserStatus(0);
            user.setUserRole(0);
            user.setInvitationCode("111");
            userList.add(user);
        }
        userService.saveBatch(userList,100);
        stopWatch.stop();
        System.out.println(stopWatch.getTotalTimeMillis());
    }

    /**
     * 批量插入用户(100000条)
     */
    @Test
    public void doInsertUsers2(){
        //帮助计时
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        final int num = 100000;
        List<User> userList = new ArrayList<>();
        for (int i = 0; i < num; i++) {
            User user = new User();
            user.setUserName("假用户");
            user.setUserAccount("fakeyupi");
            user.setAvatarUrl("https://web-memoria.oss-cn-guangzhou.aliyuncs.com/%E5%86%92%E9%99%A9%E5%B2%9B.jpeg");
            user.setGender(0);
            user.setUserPassword("123");
            user.setTags("[]");
            user.setPhone("123");
            user.setEmail("");
            user.setUserStatus(0);
            user.setUserRole(0);
            user.setInvitationCode("111");
            userList.add(user);
        }
        //14秒 10万条,1000条一批
        //userService.saveBatch(userList,1000);
        //14秒 10万条,10000条一批
        //userService.saveBatch(userList,10000);
        //14秒 10万条,50000条一批
        userService.saveBatch(userList,50000);
        stopWatch.stop();
        System.out.println(stopWatch.getTotalTimeMillis());
    }

    /**
     * 并发批量插入用户(100000条),分成10组，10000条是一组
     */
    @Test
    public void doInsertUsers3(){
        //帮助计时
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        final int num = 100000;
        //分十组
        int j = 0;
        List<CompletableFuture<Void>> futureList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            List<User> userList = Collections.synchronizedList(new ArrayList<>());
            while (true){
                j++;
                User user = new User();
                user.setUserName("假用户");
                user.setUserAccount("fakeyupi");
                user.setAvatarUrl("https://web-memoria.oss-cn-guangzhou.aliyuncs.com/%E5%86%92%E9%99%A9%E5%B2%9B.jpeg");
                user.setGender(0);
                user.setUserPassword("123");
                user.setTags("[]");
                user.setPhone("123");
                user.setEmail("");
                user.setUserStatus(0);
                user.setUserRole(0);
                user.setInvitationCode("111");
                userList.add(user);
                if (j % 10000 == 0){
                    break;
                }

            }
            //这个runAsync方法执行的操作是异步的
            CompletableFuture<Void> Future = CompletableFuture.runAsync(() -> {
                //打印当前线程的名称
                //Thread.currentThread()方法返回当前正在运行的线程的引用
                System.out.println("threadName:"+Thread.currentThread().getName());
                userService.saveBatch(userList, 1000);
            });
            //加到并发集合里，想当于拿到10个异步任务
            futureList.add(Future);

        }
        //join获取执行结果,3.6秒执行完
        CompletableFuture.allOf(futureList.toArray(new CompletableFuture[]{})).join();


        stopWatch.stop();
        System.out.println(stopWatch.getTotalTimeMillis());
    }


    /**
     * 并发批量插入用户(100000条),分成20组，5000条是一组
     */
    @Test
    public void doInsertUsers4(){
        //帮助计时
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        //分成20组
        int batchSize = 5000;
        int j = 0;
        List<CompletableFuture<Void>> futureList = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            List<User> userList = Collections.synchronizedList(new ArrayList<>());
            while (true){
                j++;
                User user = new User();
                user.setUserName("假用户");
                user.setUserAccount("fakeyupi");
                user.setAvatarUrl("https://web-memoria.oss-cn-guangzhou.aliyuncs.com/%E5%86%92%E9%99%A9%E5%B2%9B.jpeg");
                user.setGender(0);
                user.setUserPassword("123");
                user.setTags("[]");
                user.setPhone("123");
                user.setEmail("");
                user.setUserStatus(0);
                user.setUserRole(0);
                user.setInvitationCode("111");
                userList.add(user);
                if (j % batchSize == 0){
                    break;
                }

            }
            //这个runAsync方法执行的操作是异步的
            CompletableFuture<Void> Future = CompletableFuture.runAsync(() -> {
                //打印当前线程的名称
                //Thread.currentThread()方法返回当前正在运行的线程的引用
                System.out.println("threadName:"+Thread.currentThread().getName());
                userService.saveBatch(userList, batchSize);
            });
            //加到并发集合里，想当于拿到20个异步任务
            futureList.add(Future);

        }
        //join获取执行结果,3.7秒执行完
        CompletableFuture.allOf(futureList.toArray(new CompletableFuture[]{})).join();


        stopWatch.stop();
        System.out.println(stopWatch.getTotalTimeMillis());
    }

    /**
     * 并发批量插入用户(100000条),分成40组，2500条是一组
     */
    @Test
    public void doInsertUsers5(){
        //帮助计时
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        //分成40组
        int batchSize = 2500;
        int j = 0;
        List<CompletableFuture<Void>> futureList = new ArrayList<>();
        for (int i = 0; i < 40; i++) {
            List<User> userList = Collections.synchronizedList(new ArrayList<>());
            while (true){
                j++;
                User user = new User();
                user.setUserName("假用户");
                user.setUserAccount("fakeyupi");
                user.setAvatarUrl("https://web-memoria.oss-cn-guangzhou.aliyuncs.com/%E5%86%92%E9%99%A9%E5%B2%9B.jpeg");
                user.setGender(0);
                user.setUserPassword("123");
                user.setTags("[]");
                user.setPhone("123");
                user.setEmail("");
                user.setUserStatus(0);
                user.setUserRole(0);
                user.setInvitationCode("111");
                userList.add(user);
                if (j % batchSize == 0){
                    break;
                }

            }
            //这个runAsync方法执行的操作是异步的
            CompletableFuture<Void> Future = CompletableFuture.runAsync(() -> {
                //打印当前线程的名称
                //Thread.currentThread()方法返回当前正在运行的线程的引用
                System.out.println("threadName:"+Thread.currentThread().getName());
                userService.saveBatch(userList, batchSize);
            });
            //加到并发集合里，想当于拿到40个异步任务
            futureList.add(Future);

        }
        //join获取执行结果,3.75秒执行完,差不多
        CompletableFuture.allOf(futureList.toArray(new CompletableFuture[]{})).join();


        stopWatch.stop();
        System.out.println(stopWatch.getTotalTimeMillis());
    }

    /**
     * 并发批量插入用户(100000条),分成40组，2500条是一组，使用自定义线程池
     */
    @Test
    public void doInsertUsers6(){
        //帮助计时
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();



        //建议使用ThreadPoolExecutor来创建线程池
        ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(40);
        // 设置线程池的运行参数,这里设置40个
        executor.setCorePoolSize(40); //核心线程数
        executor.setMaximumPoolSize(40); //最大线程数
        executor.setKeepAliveTime(60, TimeUnit.SECONDS);  //空闲线程存活时间


        //分成40组
        int batchSize = 2500;
        int j = 0;
        List<CompletableFuture<Void>> futureList = new ArrayList<>();
        for (int i = 0; i < 40; i++) {
            List<User> userList = Collections.synchronizedList(new ArrayList<>());
            while (true){
                j++;
                User user = new User();
                user.setUserName("假用户");
                user.setUserAccount("fakeyupi");
                user.setAvatarUrl("https://web-memoria.oss-cn-guangzhou.aliyuncs.com/%E5%86%92%E9%99%A9%E5%B2%9B.jpeg");
                user.setGender(0);
                user.setUserPassword("123");
                user.setTags("[]");
                user.setPhone("123");
                user.setEmail("");
                user.setUserStatus(0);
                user.setUserRole(0);
                user.setInvitationCode("111");
                userList.add(user);
                if (j % batchSize == 0){
                    break;
                }

            }
            //这个runAsync方法执行的操作是异步的,并设置自定义线程池
            CompletableFuture<Void> Future = CompletableFuture.runAsync(() -> {
                //打印当前线程的名称
                //Thread.currentThread()方法返回当前正在运行的线程的引用
                System.out.println("threadName:"+Thread.currentThread().getName());
                userService.saveBatch(userList, batchSize);
            },executor);
            //加到并发集合里，想当于拿到40个异步任务
            futureList.add(Future);

        }
        //join获取执行结果,3.75秒执行完,差不多
        CompletableFuture.allOf(futureList.toArray(new CompletableFuture[]{})).join();

        // 当不再需要线程池时，应当关闭它
        executor.shutdown();

        stopWatch.stop();
        System.out.println(stopWatch.getTotalTimeMillis());
    }

    /**
     * 并发批量插入用户(100000条),分成20组，5000条是一组，使用自定义线程池
     */
    @Test
    public void doInsertUsers7(){
        //帮助计时
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        // 自定义线程池(不建议使用Executors创建线程池，可能会内存溢出)
        //CPU密集型:分配的核心线程数 = cpu -1
        //IO密集型: 分配的核心线程数可以大于CPU核数
        ExecutorService executorService  = new ThreadPoolExecutor(
                60,
                1000,
                10000,
                TimeUnit.MINUTES,
                new ArrayBlockingQueue<>(10000));


        //分成20组
        int batchSize = 5000;
        int j = 0;
        List<CompletableFuture<Void>> futureList = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            List<User> userList = Collections.synchronizedList(new ArrayList<>());
            while (true){
                j++;
                User user = new User();
                user.setUserName("假用户");
                user.setUserAccount("fakeyupi");
                user.setAvatarUrl("https://web-memoria.oss-cn-guangzhou.aliyuncs.com/%E5%86%92%E9%99%A9%E5%B2%9B.jpeg");
                user.setGender(0);
                user.setUserPassword("123");
                user.setTags("[]");
                user.setPhone("123");
                user.setEmail("");
                user.setUserStatus(0);
                user.setUserRole(0);
                user.setInvitationCode("111");
                userList.add(user);
                if (j % batchSize == 0){
                    break;
                }

            }
            //这个runAsync方法执行的操作是异步的,并设置自定义线程池
            CompletableFuture<Void> Future = CompletableFuture.runAsync(() -> {
                //打印当前线程的名称
                //Thread.currentThread()方法返回当前正在运行的线程的引用
                System.out.println("threadName:"+Thread.currentThread().getName());
                userService.saveBatch(userList, batchSize);
            },executorService);
            //加到并发集合里，想当于拿到40个异步任务
            futureList.add(Future);

        }
        //join获取执行结果.4秒，并没有更快
        CompletableFuture.allOf(futureList.toArray(new CompletableFuture[]{})).join();

        // 当不再需要线程池时，应当关闭它
        executorService.shutdown();

        stopWatch.stop();
        System.out.println(stopWatch.getTotalTimeMillis());
    }



}
