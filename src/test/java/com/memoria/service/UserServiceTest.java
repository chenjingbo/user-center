package com.memoria.service;

import com.baomidou.mybatisplus.core.toolkit.Assert;
import com.memoria.model.domain.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;

/**
 * @program: user-center
 * @description 用户服务测试
 * @author: memoria
 * @create: 2024-04-15 17:44
 **/
@SpringBootTest
public class UserServiceTest {

    @Autowired
    private UserService userService;

    /*@Test
    public void testAddUser(){
        User user = new User();
        user.setUserName("test001");
        user.setUserAccount("123");
        user.setAvatarUrl("123");
        user.setGender(0);
        user.setUserPassword("123");
        user.setPhone("123");
        user.setEmail("456");
        boolean save = userService.save(user);
        System.out.println(user.getId());
        Assert.isTrue(save, "");
    }*/

    /*@Test
    void userRegister() {
        String userAccount = "jklp";
        String userPassword = "";
        String checkPassword = "123456";
        String invitationCode = "1";
        long result = userService.userRegister(userAccount, userPassword, checkPassword,invitationCode);
        Assertions.assertEquals(-1 , result);
        userAccount = "jk";
        result = userService.userRegister(userAccount, userPassword, checkPassword,invitationCode);
        Assertions.assertEquals(-1 , result);
        userAccount = "jklp";
        userPassword = "123456";
        result = userService.userRegister(userAccount, userPassword, checkPassword,invitationCode);
        Assertions.assertEquals(-1 , result);
        userAccount = "123";
        result = userService.userRegister(userAccount, userPassword, checkPassword,invitationCode);
        Assertions.assertEquals(-1 , result);
        userAccount = "jk lp";
        userPassword="12345678";
        result = userService.userRegister(userAccount, userPassword, checkPassword,invitationCode);
        Assertions.assertEquals(-1 , result);
        checkPassword="123456789";
        result = userService.userRegister(userAccount, userPassword, checkPassword,invitationCode);
        Assertions.assertEquals(-1 , result);
        userAccount = "jklp";
        userPassword ="123456789";
        checkPassword="123456789";
        result = userService.userRegister(userAccount, userPassword, checkPassword,invitationCode);
        Assert.isTrue(result > 0 , "");



    }*/

    @Test
    public void testSearchUsersByTags(){
        List<String> tagNames = Arrays.asList("java","python","C++");
        List<User> userList = userService.searchUsersByTags(tagNames);
        Assert.notEmpty(userList,"判断集合是否为空");
    }

}
