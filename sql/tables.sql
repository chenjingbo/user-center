-- auto-generated definition
create table user
(
    id              bigint auto_increment comment '主键'
        primary key,
    user_name       varchar(255)                       null comment '用户名',
    user_account    varchar(255)                       null comment '登录账号',
    avatar_url      varchar(255)                       null comment '头像',
    gender          tinyint                            null,
    user_password   varchar(512)                       not null comment '密码',
    phone           varchar(128)                       null comment '电话',
    email           varchar(512)                       null,
    user_status     int      default 0                 not null comment '用户状态 0正常',
    create_time     datetime default CURRENT_TIMESTAMP null comment '创建时间',
    update_time     datetime default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP comment '更新时间',
    is_delete       tinyint  default 0                 not null comment '是否删除',
    user_role       int      default 0                 not null comment '用户角色 0普通用户，1管理员',
    invitation_code varchar(255)                       null comment '邀请码',
    tags            varchar(1024)                      null comment '标签列表'
)
    comment '用户表';

-- 队伍表
create table team
(
    id    bigint auto_increment comment '主键'
        primary key,
    name           varchar(255)                   not null comment '队伍名称',
    description    varchar(255)                   null comment '描述',
    max_num          int    default 1         not null comment '最大人数',
    expire_time     datetime                      null comment '过期时间',
    user_id        bigint                        comment '用户id(队长id)',
    status     int      default 0             not null comment '状态 0-公开，1 -私有，2-加密',
    password   varchar(512)     null comment '密码',
    create_time     datetime default CURRENT_TIMESTAMP null comment '创建时间',
    update_time     datetime default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP comment '更新时间',
    is_delete       tinyint  default 0                 not null comment '是否删除'
)
    comment '队伍表';


-- 用户-队伍表
create table user_team
(
    id    bigint auto_increment comment '主键'
        primary key,
    user_id        bigint                        comment '用户id',
    team_id        bigint                        comment '队伍id',
    join_time     datetime                      null comment '加入时间',
    create_time     datetime default CURRENT_TIMESTAMP null comment '创建时间',
    update_time     datetime default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP comment '更新时间',
    is_delete       tinyint  default 0                 not null comment '是否删除'
)
    comment '用户队伍关系';

